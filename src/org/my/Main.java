package org.my;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }
    static int DoubleSearchRec(int[] array, int key, int left, int right)
    {
        int mid=left+(right-left)/2;
        if (array[mid]==key)
            return mid;
        else if (array[mid]>key)
            return DoubleSearchRec(array, key, left, mid);
        else
            return DoubleSearchRec(array, key, mid+1, right);
    }
}
